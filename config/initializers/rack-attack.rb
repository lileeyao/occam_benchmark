class Rack::Attack
  throttle('/find', :limit => 100, :period => 5.minutes) do |req|
    if req.path == '/find' && req.post? 
      req.ip
    end
  end
end
