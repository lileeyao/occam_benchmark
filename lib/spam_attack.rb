require 'net/http'
require 'json'
require 'byebug'
require 'faraday'
require 'rack'
require 'rack/server'
require 'benchmark'
require 'ruby-prof'

class SpamAttack
  def call
    n = 1000
    in_func
=begin
    Benchmark.bm do |x|
      x.report {
        for i in 1..n
          runner
        end
      }
    end

    result = RubyProf.profile do
      for i in 1..n
        runner
      end
    end

    printer = RubyProf::GraphPrinter.new(result)
    printer.print(STDOUT)
=end
      runner
  end 

  def in_func
    @request = {"account":{"id":621,"shard_id":1},
      "user":{"id":-1,"groups":[-1],"role":"agent","organizations":[],"ticket_access":"all"},
      "rule":{"id":21572762,"type":"View","all":[{"field":"status_id","operator":"less_than","value":"4"}],"any":[]},
      "context":{"ticket_custom_fields":[{"id":20007502,"type":"FieldText","tag": nil}],"user_custom_fields":[],"organization_custom_fields":[],"available_languages":[["Português (Brasil)",42],["English",1],["Français",15],["日本語 (Japanese)",43]],"use_status_hold?":false},
      "options":{"order":["nice_id desc"],"limit":1000,"offset":nil,"with_archived":false,"tag_evaluation_optimization?":true}}.to_json


    @conn = Faraday.new(:url => 'http://localhost:3210') do |faraday|
      faraday.request :url_encoded
      faraday.response :logger
      faraday.adapter Faraday.default_adapter
    end
  end

  def runner
    begin 
      @conn.post do |req|
        req.url '/find'
        req.headers['Content-Type'] = 'application/json'
        req.body = @request
      end
    rescue Exception => e
      puts e
    end
  end
end


class SpamAttackApp
  def initialize(app)
    @app = app
  end
  def self.call(env)
    sa = SpamAttack.new 
    ret = sa.call
    [200, {}, env.inspect]
  end
end

