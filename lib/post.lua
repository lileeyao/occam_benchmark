wrk.scheme = "http"
wrk.host = "localhost"
wrk.port = nil
wrk.method = "POST"
wrk.path = "/find"
wrk.headers["Content-Type"] = "application/json"
wrk.body = "{\"account\":{\"id\":42,\"shard_id\":1},\"user\":{\"id\":-1,\"groups\":[-1],\"role\":\"agent\",\"organizations\":[],\"ticket_access\":\"all\"},\"rule\":{\"id\":11101, \"type\":\"View\",\"all\":[{\"field\":\"status_id\",\"operator\":\"less_than\",\"value\":\"4\"}],\"any\":[]},\"context\":{\"ticket_custom_fields\":[],\"user_custom_fields\":[],\"organization_custom_fields\":[],\"use_status_hold?\":false},\"options\":{\"order\":[\"nice_id desc\"],\"limit\":1000,\"offset\":null,\"with_archived\":false}}"
