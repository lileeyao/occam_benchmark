require 'bundler'
Bundler.require 

require File.join(File.dirname(__FILE__),'lib', 'spam_attack')

app = Rack::Builder.new do
  run SpamAttackApp
end

Rack::Server.start :app => app
